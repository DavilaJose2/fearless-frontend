window.addEventListener('DOMContentLoaded', async () => {

    const locationsUrl = 'http://localhost:8000/api/locations/';
    const locationsResponse = await fetch(locationsUrl);
    if (locationsResponse.ok) {
      const locationsData = await locationsResponse.json();
      const selectTag = document.getElementById('location');
      for (let location of locationsData.locations) {
        console.log(location);
        const optionElement = document.createElement('option');

        optionElement.value = location.id;

        optionElement.innerHTML = location.name;
        selectTag.appendChild(optionElement);
      }
    }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));
      console.log(json);
      const conferenceUrl = 'http://localhost:8000/api/conferences/';
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(conferenceUrl, fetchConfig);
      if (response.ok) {
        formTag.reset();
        const newConference = await response.json();
        console.log(newConference);
      }
    });
  });
