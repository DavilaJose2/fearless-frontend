import React from 'react';
import { NavLink } from 'react-router-dom';
import './Nav.css';

function Nav() {
    return (
        <nav className="nav-header">
            <NavLink className="nav-link" aria-current="page" to="/">Home</NavLink>
            <NavLink className="nav-link" aria-current="page" to="/attendees">Attendees</NavLink>
            <NavLink className="nav-link" aria-current="page" to="/attendees/new">New Attendee</NavLink>
            <NavLink className="nav-link" aria-current="page" to="/locations/new">New Location</NavLink>
            <NavLink className="nav-link" aria-current="page" to="/conferences/new">New Conference</NavLink>
            <NavLink className="nav-link" aria-current="page" to="/presentations/new">New Presentation</NavLink>
        </nav>
    );
}

export default Nav;
