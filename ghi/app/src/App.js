import { BrowserRouter, Routes, Route } from "react-router-dom";
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import LocationForm from './LocationForm';
import AttendeesList from './AttendeesList';
import PresentationForm from './PresentationForm';
import Nav from './Nav';
import MainPage from "./MainPage";



function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />

        <Routes>
          <Route index element={<MainPage />} />
          <Route path="/conferences/new" element={<ConferenceForm />} />
          <Route path="/attendees/new" element={<AttendConferenceForm />} />
          <Route path="/locations/new" element={<LocationForm />} />
          <Route path="/attendees" element={<AttendeesList attendees = {props.attendees}/>} />
          <Route path="/presentations/new" element={<PresentationForm/>} />
          
        </Routes>

    </BrowserRouter>
  );
}
export default App;
