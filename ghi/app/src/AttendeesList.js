

// function AttendeesList(props) {
//   if (!Array.isArray(props.attendees) || props.attendees.length === 0) {
//     return <div>No attendees available</div>;
//   }

//   return (
//     <table className="table table-striped">
//       <thead>
//         <tr>
//           <th scope="col" > Name</th>
//           <th scope="col" >Conference</th>
//         </tr>
//       </thead>
//       <tbody>
//         {props.attendees.map(attendee =>{
//           return(
//           <tr key={attendee.href}>
//             <td>{attendee.name}</td>
//             <td>{attendee.conference}</td>
//           </tr>
//           )

//     })}
//       </tbody>
//     </table>
//   );
// }

// export default AttendeesList;


function AttendeesList (props) {
  return(
  <table className="table table-striped">
      <thead>
          <tr>
              <th scope="col">Name</th>
              <th scope="col">Conference</th>
          </tr>
      </thead>
      <tbody>
      {props.attendees.map(attendee => {
          return (
          <tr key={attendee.href}>
              <td>{attendee.name}</td>
              <td>{attendee.conference}</td>
          </tr>

          )
      })}
      </tbody>

  </table>
  );
}
export default AttendeesList
