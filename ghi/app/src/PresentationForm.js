import React, { useState, useEffect,  } from 'react';


function PresentationForm() {
    // State variables
    const [title, setTitle] = useState('');
    const [presenterName, setPresenterName] = useState('');
    const [presenterEmail, setPresenterEmail] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [synopsis, setSynopsis] = useState('');
    const [conference, setConference] = useState('');
    const [conferences, setConferences] = useState([]);

    // Fetch conferences on component mount
    useEffect(() => {
        const fetchConferences = async () => {
            try {
                const response = await fetch('http://localhost:8000/api/conferences/');
                if (response.ok) {
                    const data = await response.json();
                    setConferences(data.conferences);
                }
            } catch (error) {
                console.error('Error fetching conferences:', error);
            }
        };
        fetchConferences();
    }, []);

    // Handle form submission
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = { title, presenterName, presenterEmail, companyName, synopsis, conference };
        console.log(data); // Implement the POST request logic here

        // Reset form fields after successful submission
        setTitle('');
        setPresenterName('');
        setPresenterEmail('');
        setCompanyName('');
        setSynopsis('');
        setConference('');
    };

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new presentation</h1>
                        <form onSubmit={handleSubmit}>
                            {/* Presenter Name */}
                            <div className="form-floating mb-3">
                                <input
                                    type="text"
                                    className="form-control"
                                    id="presenter_name"
                                    placeholder="Presenter name"
                                    value={presenterName}
                                    onChange={e => setPresenterName(e.target.value)}
                                    required
                                />
                                <label htmlFor="presenter_name">Presenter name</label>
                            </div>

                            {/* Presenter Email */}
                            <div className="form-floating mb-3">
                                <input
                                    type="email"
                                    className="form-control"
                                    id="presenter_email"
                                    placeholder="Presenter email"
                                    value={presenterEmail}
                                    onChange={e => setPresenterEmail(e.target.value)}
                                    required
                                />
                                <label htmlFor="presenter_email">Presenter email</label>
                            </div>

                            {/* Company Name */}
                            <div className="form-floating mb-3">
                                <input
                                    type="text"
                                    className="form-control"
                                    id="company_name"
                                    placeholder="Company name"
                                    value={companyName}
                                    onChange={e => setCompanyName(e.target.value)}
                                />
                                <label htmlFor="company_name">Company name</label>
                            </div>

                            {/* Title */}
                            <div className="form-floating mb-3">
                                <input
                                    type="text"
                                    className="form-control"
                                    id="title"
                                    placeholder="Title"
                                    value={title}
                                    onChange={e => setTitle(e.target.value)}
                                    required
                                />
                                <label htmlFor="title">Title</label>
                            </div>

                            {/* Synopsis */}
                            <div className="mb-3">
                                <label htmlFor="synopsis">Synopsis</label>
                                <textarea
                                    className="form-control"
                                    id="synopsis"
                                    rows="3"
                                    value={synopsis}
                                    onChange={e => setSynopsis(e.target.value)}
                                ></textarea>
                            </div>

                            {/* Conference Selection */}
                            <div className="mb-3">
                                <select
                                    className="form-select"
                                    id="conference"
                                    value={conference}
                                    onChange={e => setConference(e.target.value)}
                                    required
                                >
                                    <option value="">Choose a conference</option>
                                    {conferences.map(c => (
                                        <option key={c.id} value={c.id}>{c.name}</option>
                                    ))}
                                </select>
                            </div>

                            <button type="submit" className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default PresentationForm;
